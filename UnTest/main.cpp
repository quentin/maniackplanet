#define NOMINMAX
#define BUFSIZE 4096

#include <iostream>
#include <Windows.h>
#include <tchar.h>
#include <limits>
#include "main.h"

// Source : http://blog.opensecurityresearch.com/2013/01/windows-dll-injection-basics.html
int main(int argc, char* argv[]) {
	int pid;
	DWORD dwBytesRead;
	HANDLE hHandle;
	HANDLE hRemoteThread;
	LPVOID remoteDllAddr;
	FARPROC loadLibAddr;
	TCHAR dllPath[BUFSIZE] = TEXT("");

	std::cout << "Enter PID: ";
	std::cin >> pid;

	// [1] Attaching to the Process
	hHandle = OpenProcess(
		PROCESS_CREATE_THREAD |
		PROCESS_QUERY_INFORMATION |
		PROCESS_VM_OPERATION |
		PROCESS_VM_WRITE |
		PROCESS_VM_READ,
		FALSE,
		pid
	);

	// [1.1] Check if OpenProcess succeed
	if (hHandle == NULL) {
		std::cout << "Unable to open process. Error code: " << GetLastError() << std::endl;
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cin.get();
		return 1;
	}

	// [2] Allocating memory - using DLL Path

	// [2.1] Get absolute path to the DLL from the given relative path
	GetFullPathName(TEXT("../Debug/Observer.dll"), BUFSIZE, dllPath, NULL);
	std::cout << "Generated full path name is: " << dllPath << std::endl;

	// [2.2] Allocate memory in our remote process to push the string containing the path of our dll
	remoteDllAddr = VirtualAllocEx(hHandle, NULL, strlen(dllPath), MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);

	// [3] Copying the DLL / Determine Addresses
	
	// [3.1] Copy the absolute path of our dll to the remote process.
	WriteProcessMemory(hHandle, remoteDllAddr, dllPath, strlen(dllPath), NULL);

	// [3.2] Get the address of the LoadLibraryA function contained in kernel32.dll which is loaded by every apps.
	loadLibAddr = GetProcAddress(GetModuleHandle(TEXT("kernel32.dll")), "LoadLibraryA");

	// [4] Executing the DLL!
	// We call LoadLibraryA with our dllPath as parameter in our remote Process.
	hRemoteThread = CreateRemoteThread(hHandle, NULL, 0, (LPTHREAD_START_ROUTINE)loadLibAddr, remoteDllAddr, 0, NULL);
	WaitForSingleObject(hRemoteThread, INFINITE);

	/*std::cout << "Press Enter to Continue";
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	std::cin.get();*/

	return 0;
}