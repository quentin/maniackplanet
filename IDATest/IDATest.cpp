// IDATest.cpp�: d�finit le point d'entr�e pour l'application console.
//

#include "stdafx.h"
#include <iostream>

class Toto {
public:
	int age;
	Toto(int _age) : age(_age) {
		std::cout << "Toto::Toto(int)" << std::endl;
	};

	void grandir(int nA) {
		std::cout << "Toto::grandir(int)" << std::endl;
		age += nA;
	}
};

int main()
{
	Toto a = Toto(5);
	a.grandir(10);
	std::cin.get();
    return 0;
}

